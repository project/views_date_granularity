Views Date Granularity
----
Views contextual filter for Date fields.
Support only Date unix type fields.


This module requires the following modules:
 * Date (https://drupal.org/project/date)

Available contextual filters:
 * Date in the form of YYYY.
 * Date in the form of MM (01 - 12).
 * Date in the form of DD (01 - 31).
 * Date in the form of WW (01 - 53).
 * Date in the form of YYYYMM.
 * Date in the form of CCYYMMDD.

