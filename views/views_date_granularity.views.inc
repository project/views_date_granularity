<?php
/**
 * @file
 * Contains Views hooks.
 */

/**
 * Implements hook_field_views_data_alter()
 */
function views_date_granularity_field_views_data_alter(&$result, $field, $module) {
  if ($module != 'date') {
    return NULL;
  }

  $has_end_date = !empty($field['settings']['todate']);
  $labels = field_views_field_label($field['field_name']);
  $label = array_shift($labels);

  foreach ($result as $table_name => $table_data) {
    foreach ($field['columns'] as $column_name => $column_attributes) {
      $settings = [
        'column_name' => $column_name,
        'column_attributes' => $column_attributes,
        'has_end_date' => $has_end_date,
        'label' => $label,
        'additional_label' => '',
      ];

      if ($column_name == 'value2' && !$has_end_date) {
        // Skip add arguments if user disable todate for field.
        continue;
      }

      if ($has_end_date) {
        if ($column_name == 'value') {
          $settings['additional_label'] = ' - start date';
        }
        elseif ($column_name == 'value2') {
          $settings['additional_label'] = ' - end date';
        }
      }

      $column_arguments = views_date_granularity_add_views_date_column_arguments($field, $settings);
      $table_data = $table_data + $column_arguments;
    }
    $result[$table_name] = $table_data;
  }
}

/**
 * For date field table, add views contextual argument.
 *
 * @param $field
 * @param $settings
 *
 * @return array
 */
function views_date_granularity_add_views_date_column_arguments($field, $settings) {
  $views_column_arguments = [];
  if (isset($settings['column_attributes']['type'])) {
    switch ($settings['column_attributes']['type']) {
      case 'int':
        $views_column_arguments = views_date_granularity_add_views_date_unix_arguments($field, $settings);
        break;
    }
  }
  return $views_column_arguments;
}

/**
 * Views contextual argument for date unix field.
 *
 * @param $field
 * @param $settings
 *
 * @return array
 */
function views_date_granularity_add_views_date_unix_arguments($field, $settings) {
  $sql_column_name = _field_sql_storage_columnname($field['field_name'], $settings['column_name']);
  $views_column_arguments = array();

  foreach (views_date_granularity_get_date_arguments_granularity() as $argument_type => $help_text) {
    $views_column_arguments[$sql_column_name. '_' . $argument_type] = array(
      'title' => t('[@argument] @label@additional_label (!name)', [
        '@label' => $settings['label'],
        '@argument' => $argument_type,
        '!name' => $field['field_name'],
        '@additional_label' => $settings['additional_label'],
       ]),
      'title short' => t('[@argument] @label@additional_label', [
        '@label' => $settings['label'],
        '@argument' => $argument_type,
        '@additional_label' => $settings['additional_label'],
      ]),
      'group' => t('Views Date Granularity'),
      'help' => $help_text,
      'argument' => array(
        'field' => $sql_column_name,
        'handler' => 'views_handler_argument_views_date_granularity_' . $argument_type,
      ),
    );
  }

  return $views_column_arguments;
}

/**
 * Available granularity for contextual arguments in date.
 *
 * @return array
 */
function views_date_granularity_get_date_arguments_granularity() {
  $arguments = [
    // Argument type => help text.
    'year' => t('Date in the form of YYYY.'),
    'month' => t('Date in the form of MM (01 - 12).'),
    'day' => t('Date in the form of DD (01 - 31).'),
    'week' => t('Date in the form of WW (01 - 53).'),
    'year_month' => t('Date in the form of YYYYMM.'),
    'full_date' => t('Date in the form of CCYYMMDD.'),
  ];
  return $arguments;
}
