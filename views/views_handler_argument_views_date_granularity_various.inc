<?php

/**
 * @file
 * Handlers for various date arguments.
 *
 * @ingroup views_argument_handlers
 */

/**
 * Argument handler for a full date (CCYYMMDD)
 */
class views_handler_argument_views_date_granularity_fulldate extends views_handler_argument_node_created_fulldate {
}

/**
 * Argument handler for a year (CCYY)
 */
class views_handler_argument_views_date_granularity_year extends views_handler_argument_node_created_year {
}

/**
 * Argument handler for a year plus month (CCYYMM)
 */
class views_handler_argument_views_date_granularity_year_month extends views_handler_argument_node_created_year_month {
}

/**
 * Argument handler for a month (MM)
 */
class views_handler_argument_views_date_granularity_month extends views_handler_argument_node_created_month {
}

/**
 * Argument handler for a day (DD)
 */
class views_handler_argument_views_date_granularity_day extends views_handler_argument_node_created_day {
}

/**
 * Argument handler for a week.
 */
class views_handler_argument_views_date_granularity_week extends views_handler_argument_node_created_week {
}
